
//-------------------------------------------------//
// ------------  Our Service Section --------------//
//-------------------------------------------------//

    const $ourServicesButton = $('.ourServicesButton');
    const $tabs = $('.ourServicesData div[data-tab]');

    $ourServicesButton.on('click', function () {
        const serviceTabIndex = $(this).index();
        $ourServicesButton.removeClass('ourSerActiveBtn');
        $(this).addClass('ourSerActiveBtn');
        $tabs.each(function (index) {
            if (index === serviceTabIndex) {
                $tabs.eq(index).css({'display': 'flex'});
            } else {
                $tabs.eq(index).css({'display': 'none'});
            }
        })
    });

//-------------------------------------------------//
// ------------  Our Amazing Section --------------//
//-------------------------------------------------//

// Tabs switch
    const $ourAmazingButton = $('.ourAmazingButton');
    let $ourAmazingImgs = $('.ourAmazingWrapper figure');

    $ourAmazingButton.on('click', function () {
        let selectedIMGs = $(this).data().btnid;
        $ourAmazingButton.removeClass('ourAmazingActiveBtn');
        $(this).addClass('ourAmazingActiveBtn');

        if (selectedIMGs !== 'All') {
            $ourAmazingImgs.not(`[data-btnID='${selectedIMGs}']`).css({'display': 'none'});
            $(`figure[data-btnID='${selectedIMGs}']`).css({'display': 'flex'});
        } else {
            $ourAmazingImgs.css({'display': 'flex'});
        }
    });

// Load More Images Using Load Button
    const $amazingWorkBtn = $('.amazingWorkBtn');
    const $loading = $('.loading');
    const $secondImgBlock = $('.ourAmazingImgsBlock-Second figure');
    const $thirdImgBlock = $('.ourAmazingImgsBlock-Third figure');

    let amazingCounter = 0;

    $amazingWorkBtn.on('click', function (event) {
        setTimeout(function () {
            if (amazingCounter === 0) {
                $secondImgBlock.unwrap();
                amazingCounter++;
                $amazingWorkBtn.css({'opacity': '1'});
            } else if (amazingCounter === 1) {
                $thirdImgBlock.unwrap();
                amazingCounter++;
                $amazingWorkBtn.remove()
            }
            $loading.css({'display': 'none'});
        }, 2000);

        $loading.css({'display': 'unset'});
        $amazingWorkBtn.css({'opacity': '0'});
    });

//-------------------------------------------------//
// ------------ What people Say (Slider) --------------//
//-------------------------------------------------//

    const $slider = $('.slider');

    $slider.slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        speed: 400,
        draggable: false,
    });

    $('.sliderBig').slick({
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        initialSlide: 2,
        speed: 400,
        draggable: false,
    });

    $('.slider div[data-slick-index="2"]').addClass('slideInFocus');

    $slider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {

        $('.sliderItem').removeClass('slideInFocus');
        $(`div[data-slick-index="${nextSlide + 2}"]`).addClass('slideInFocus');
        $(`p[data-pID="${nextSlide + 2}"]`).addClass('pInFocus');
        $('.commentBox[data-pID]').removeClass('activeCommentBox').removeClass('animatedActiveCommentBox').css({'opacity': '0'});
        $(`.commentBox[data-pID="${nextSlide}"]`).addClass('animatedActiveCommentBox').animate({
            'opacity': '1',
        }, 400);
        $('.sliderBig').slick('slickGoTo', nextSlide + 2);
    });

    $('.slider img').on('click', function (event) {
        let index = $(event.target).parent().data('slickIndex') - 2;
        if (index === -2) {
            $('.slider').slick('slickPrev');
            setTimeout(function () {
                $('.slider').slick('slickPrev')
            },400)
        }
        $slider.slick('slickGoTo', index);
    });

//-------------------------------------------------//
    // ------------ Masonry --------------//
//-------------------------------------------------//

let $grid;

$(window).on("load", function () {

        $grid = $('.grid').masonry({
        itemSelector: '.grid-item',
        gutter: '.gutter-sizer',
        percentPosition: true,
        horizontalOrder: true,
    });


    $('.gridTwo').masonry({
        itemSelector: '.gridTwo-item',
        gutter: '.gutterForGridTwo-item',
        percentPosition: true,
    });


    $('.gridThree').masonry({
        itemSelector: '.gridThree-item',
        percentPosition: true,
        horizontalOrder: true,
        gutter: '.gutterForGridThree-item',
    });
});

//--------------------------------------------------------------------------//
    // ------------ Masonry Gallary Button functionality --------------//
//--------------------------------------------------------------------------//

    const $galleryButton = $('.galleryButton');
    const $loadingGallery = $('.loadingGallery');
    let amazingCounterGallery = 0;

    function createElement(arr) {
        let $element = $(`
                    <div class="grid-item grid-itemW2">
                        <div class="hoverButtons">
                            <span><i class="fas fa-search"></i></span>
                            <span><i class="fas fa-compress-arrows-alt"></i></span>
                        </div>
                        <img src="./images/masondry/${arr[0]}pint_IMG.jpg" alt="grid-img">
                    </div>

                    <div class="grid-item">
                        <div class="hoverButtons">
                            <span><i class="fas fa-search"></i></span>
                            <span><i class="fas fa-compress-arrows-alt"></i></span>
                        </div>
                        <img src="./images/masondry/${arr[1]}pint_IMG.jpg" alt="grid-img">
                    </div>

                    <div class="grid-item">
                        <div class="hoverButtons">
                            <span><i class="fas fa-search"></i></span>
                            <span><i class="fas fa-compress-arrows-alt"></i></span>
                        </div>
                        <img src="./images/masondry/${arr[2]}pint_IMG.jpg" alt="grid-img">
                    </div>
        `);
        return $element
    }

    $galleryButton.on('click', function (event) {
        setTimeout(function () {

            if (amazingCounterGallery === 0) {
                const $element = createElement([1, 2, 3]);
                $grid.append($element).masonry('appended', $element);
                amazingCounterGallery++;
                $galleryButton.css({'opacity': '1'});
                $grid.imagesLoaded( function() {
                    $grid.masonry();
                });
            } else if (amazingCounterGallery === 1) {
                $grid.masonry('reloadItems');
                const $element =createElement([4, 5, 6]);
                $grid.append($element).masonry('appended', $element);
                amazingCounterGallery++;
                $galleryButton.remove();
                $grid.imagesLoaded( function() {
                    $grid.masonry();
                });
            }
            $loadingGallery.css({'display': 'none'});
        }, 2000);

        $loadingGallery.css({'display': 'unset'});
        $galleryButton.css({'opacity': '0'});
    });





